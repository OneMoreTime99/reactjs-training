#REACTJS TRAINING

1. Fundamental topic
- Create React App
- JSX
- Rendering Element
- Components: Functional/ Class Components
- Managing State and Props
- Component Life Cycle
- Handling Event
- Condition Rendering
- Lists and Keys
- Composition and Inheritance
- Basic Hooks: useState, useEffect
- Routing
- API Calls
- Form: Formik
- State Management: Redux, Redux Toolkit
- Testing