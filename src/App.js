import React, { Component } from "react";
import ChangeColorFontSize from "./views/ClassComponents/ChangeColorFontSize";
import ColorBox from "./views/ClassComponents/ChangeColorBox/index";
import Clock from "./views/ClassComponents/Clock";
import Todo from "./views/ClassComponents/Todo";
import FilterPostLists from "./views/ClassComponents/FilterPostLists";
import Hobby from "./views/FunctionComponents/Hobby";
import Routing from "./views/FunctionComponents/Routing";
class App extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">Change Color FontSize</h3>
            </div>
            <div className="panel-body">
              <ChangeColorFontSize />
              <hr />
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">Change Color box</h3>
            </div>
            <div className="panel-body">
              <ColorBox />
              <hr />
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">Clock</h3>
            </div>
            <div className="panel-body">
              <Clock />
              <hr />
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">Todo list</h3>
            </div>
            <div className="panel-body">
              <Todo />
              <hr />
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">Filter - pagination - api - debound</h3>
            </div>
            <div className="panel-body">
              <FilterPostLists />
              <hr />
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">Redux</h3>
            </div>
            <div className="panel-body">
              <Hobby />
              <hr />
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="panel panel-success">
            <div className="panel-heading">
              <h3 className="panel-title">Routing</h3>
            </div>
            <div className="panel-body">
              <Routing />
              <hr />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App;
