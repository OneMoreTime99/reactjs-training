import React, { Component } from "react";

class Clock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timeString: '',
            clock: 'Hide Clock',
            statusClock: true
        }
    }

    formatDate(date) {

        if (!date) return ''

        const hours = `0${date.getHours()}`.slice(-2)
        const minitue = `0${date.getMinutes()}`.slice(-2)
        const second = `0${date.getSeconds()}`.slice(-2)

        return `${hours}:${minitue}:${second}`
    }

    componentDidMount() {
        setInterval(() => {
            const now = new Date();
            const newTimeString = this.formatDate(now)
            this.setState({
                timeString: newTimeString
            })
        }, 1000);
    }

    setShowClock(boolean) {
        if (boolean) {
            this.setState({
                clock: 'Show Clock',
                statusClock: false
            })
        } else {
            this.setState({
                clock: 'Hide Clock',
                statusClock: true
            })
        }
    }

    render() {
        return (
            <div>
                <p style={{ fontSize: '42px' }}>{this.state.statusClock && this.state.timeString}</p>
                <button onClick={() => this.setShowClock(this.state.statusClock)}>{this.state.clock}</button>
            </div>
        );
    }
}

export default Clock;