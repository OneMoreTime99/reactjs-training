import React, { Component } from "react";
import TodoForm from "../../components/ClassComponents/Todo/TodoForm";
import TodoLists from "../../components/ClassComponents/Todo/TodoLists";

class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lists: [
                { id: 1, title: "I love you 3000" },
                { id: 2, title: "Keep innovating" },
                { id: 3, title: "We are one" }
            ]
        }
    }

    handleTodoFormSubmit = (formValue) => {
        const newTodo = {
            id: this.state.lists.length + 1,
            ...formValue
        }
        const newTodoLists = [...this.state.lists]
        newTodoLists.push(newTodo)

        this.setState({
            lists: newTodoLists
        }, () => {
            console.log(this.state.lists)
        })

    }

    handleTodoClick = (todo) => {
        console.log(todo)
        console.log(this.state.lists)
        const index = this.state.lists.findIndex((x) => x.id === todo.id);
        if (index < 0) return;
        const newTodoLists = [...this.state.lists];
        newTodoLists.splice(index, 1);
        this.setState({
            lists: newTodoLists
        })
    }

    render() {
        return (
            <div>
                <TodoForm onSubmit={this.handleTodoFormSubmit} />
                <TodoLists todos={this.state.lists} onTodoClick={this.handleTodoClick} />
            </div>
        );
    }
}

export default Todo;