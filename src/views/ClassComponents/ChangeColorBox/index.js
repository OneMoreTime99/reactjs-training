import React, { Component } from "react";
import './style.css';

class changeColorBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            color: localStorage.getItem('box_color') || 'deeppink'
        };
        this.handleBoxClick = this.handleBoxClick.bind(this)
    }

    getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    handleBoxClick() {
        const newColor = this.getRandomColor();
        this.setState({
            color: newColor
        })
        localStorage.setItem('box_color', newColor)
    }

    render() {
        return (

            <div className="color-box" style={{ background: this.state.color }} onClick={this.handleBoxClick}>
                Click me to change color!
            </div>
        );
    }
}

export default changeColorBox;