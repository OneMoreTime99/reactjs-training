import React, { Component } from "react";
import PostList from "../../components/ClassComponents/FilterPostList/PostList";
import queryString from "query-string";
import postApi from "../../api/PostApi"
import PostFilter from "../../components/ClassComponents/FilterPostList/PostFilter";
import Pagination from "../../components/ClassComponents/FilterPostList/Pagination";

class FilterPostLists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postLists: [],
            filters: {
                _page: 1,
                _limit: 10,
                title_like: ''
            },
            pagination: {
                _page: 1,
                _limit: 10,
                _totalRows: 1,
            },
            totalPage: 1
        };
        this.handlePageChange = this.handlePageChange.bind(this)
    }

    fetchPostList = async (fiter) => {
        try {
            const paramString = queryString.stringify(fiter);
            const requestUrl = `http://js-post-api.herokuapp.com/api/posts?${paramString}`;
            const response = await fetch(requestUrl);
            const responseJSON = await response.json();
            const { data, pagination } = responseJSON;
            this.setState({
                postLists: data,
                pagination: pagination
            }, () => {
                this.setState({
                    totalPage: Math.ceil(this.state.pagination._totalRows / this.state.pagination._limit)
                }, () => {
                    console.log(this.state.totalPage)
                })

            })
        } catch (error) {
            console.log("Failed to fetch post list:", error)
        }
    }

    componentDidMount() {
        this.fetchPostList(this.state.filters)
    }

    handleFilterChange = (newFilter) => {
        this.setState({
            filters: {
                ...this.state.filters,
                _page: 1,
                title_like: newFilter.searchTerm
            }
        }, () => {
            this.fetchPostList(this.state.filters)
        })
    }

    handlePageChange(newPage) {
        this.setState({
            filters: {
                ...this.state.filters,
                _page: newPage
            }
        }, () => {
            this.fetchPostList(this.state.filters)
        })
    }

    render() {
        return (
            <div>
                <PostFilter onSubmit={this.handleFilterChange} />
                <PostList posts={this.state.postLists} />
                <Pagination totalPage={this.state.totalPage} pagination={this.state.pagination} onPageChange={this.handlePageChange} />
            </div>
        );
    }
}

export default FilterPostLists;