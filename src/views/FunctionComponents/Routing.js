import Nav from '../../components/FunctionComponents/Router/Nav'
import Shop from '../../components/FunctionComponents/Router/Shop';
import About from '../../components/FunctionComponents/Router/About';
import Home from '../../components/FunctionComponents/Router/Home';
import ItemDetail from '../../components/FunctionComponents/Router/ItemDetail'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

function Routing() {
    return (
        <Router>
            <div className="App">
                <Nav />
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/shop" exact component={Shop} />
                    <Route path="/about" component={About} />
                    <Route path="/shop/:id" component={ItemDetail} />
                </Switch>
            </div>
        </Router>
    );
}

export default Routing;