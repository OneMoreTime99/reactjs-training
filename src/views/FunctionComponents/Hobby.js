import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import HobbyList from '../../components/FunctionComponents/HobbyLists/Index';
// import casual from 'casual';
import { useDispatch } from 'react-redux';
import { addNewHobby, setActiveHobby } from '../../action/Hobby';

Homepage.propTypes = {

};

Homepage.default = {

}

// Tạo gia các giá trị ngẫu nhiên gồm 4 chữ số
const randomNumber = () => {
    return 1000 + Math.trunc(Math.random() * 9000)
}

function Homepage(props) {
    // Ở đây sẽ dùng hook useSelector để lấy ra 
    const hobbyList = useSelector(state => state.hobby.list);
    const activeId = useSelector(state => state.hobby.activeId);
    const dispatch = useDispatch();

    const handleAddHobbyList = () => {
        //Random hobby object: id + title
        const newId = randomNumber()
        const newHobby = {
            id: newId,
            title: `Hobby ${newId}`,
        }

        //Dispatch action to add a new hobby to redux store
        const action = addNewHobby(newHobby);
        dispatch(action);
    }

    const handleHobbyList = (hobby) => {
        const action = setActiveHobby(hobby);
        dispatch(action);
    }

    return (
        <div className="home-page">
            <button onClick={handleAddHobbyList}>Random hobby </button>
            <HobbyList hobbyList={hobbyList} activeId={activeId} onHobbyClick={handleHobbyList}></HobbyList>
        </div>
    );
}

export default Homepage;