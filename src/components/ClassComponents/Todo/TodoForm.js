import React, { Component } from "react";

class TodoForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }

    handleValueChange = (e) => {
        this.setState({
            value: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        if (this.state.value.length < 1) {
            return
        } else {
            const formValue = {
                title: this.state.value
            }
            this.props.onSubmit(formValue);
            this.setState({
                value: ''
            })
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <p>Enter title new:</p>
                <input
                    type='text'
                    onChange={this.handleValueChange}
                />
                <input type='submit' />
            </form>
        );
    }
}

export default TodoForm;