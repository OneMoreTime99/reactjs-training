import React, { Component } from "react";

class TodoLists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: this.props.todos
        }
    }

    handleClick = (todo) => {
        if (this.props.onTodoClick) {
            this.props.onTodoClick(todo)
        }
    }

    render() {
        return (
            <ul className="todo-list">
                {this.props.todos.map(todo => (
                    <li key={todo.id} onClick={() => this.handleClick(todo)} style={{ width: 'fit-content' }}>{todo.title}</li>
                ))
                }
            </ul>
        );
    }
}

export default TodoLists;