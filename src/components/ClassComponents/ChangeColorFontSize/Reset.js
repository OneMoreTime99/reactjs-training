import React, { Component } from "react";

class Reset extends Component {

    onResetDefaul(value) {
        this.props.onSettingDefault(value);
    }

    render() {
        return (
            <button type="button" className="btn btn-primary" onClick={() => this.onResetDefaul(true)}>reset</button>
        )
    }
};

export default Reset;