import React, { Component } from "react";

class PostList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: []
        }
    }

    render() {
        return (
            <ul className="post-list">
                {this.props.posts.map(post => (
                    <li key={post.id}>{post.title}</li>
                ))}
            </ul>
        );
    }
}

export default PostList;