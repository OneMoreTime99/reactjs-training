import React, { Component } from "react";

class Pagination extends Component {
    constructor(props) {
        super(props);
        this.state = {
            totalPage: this.props.totalPage
        }
    }

    handlePageChange = (newPage) => {
        console.log(newPage)
        if (this.props.onPageChange) {
            this.props.onPageChange(newPage)
        }
    }

    render() {
        return (
            <div>
                <button
                    disabled={this.props.pagination._page <= 1}
                    onClick={() => this.handlePageChange(this.props.pagination._page - 1)}>
                    Prev
                </button>
                <button
                    disabled={this.props.pagination._page >= this.props.totalPage}
                    onClick={() => this.handlePageChange(this.props.pagination._page + 1)}>
                    Next
                </button>
            </div>
        );
    }
}

export default Pagination;