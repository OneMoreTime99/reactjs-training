import React, { Component } from "react";
import { debounce, throttle } from 'lodash'

class PostFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchItem: ''
        };
        this.throttleHandleChange = debounce(this.throttleHandleChange.bind(this), 300)
    }

    throttleHandleChange(edata) {
        const formValue = {
            searchTerm: edata
        }
        this.props.onSubmit(formValue)
    }

    handleSearchTermChange = (e) => {
        this.setState({
            searchItem: e.target.value
        }, () => {
            this.throttleHandleChange(this.state.searchItem)
        })



    }

    render() {
        return (
            <form>
                <p>Enter title new:</p>
                <input
                    type='text'
                    onChange={this.handleSearchTermChange}
                />
            </form>
        );
    }
}

export default PostFilter;