import {useState, useEffect} from 'react';

function Shop({match}){

    useEffect(() => {
        fetchItem()
    }, [])

    const [item, setItem] = useState({})

    const fetchItem = async () => {
        const data = await fetch(`https://gorest.co.in/public/v1/users?id=${match.params.id}`)

        const item = await data.json()
        setItem(item.data[0])
    }

    return (
        <div>
            <h1>{item.name}</h1>
        </div>
    );
}

export default Shop;
