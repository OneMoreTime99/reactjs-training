import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

function Shop(){

    useEffect(() => {
        fetchItem()
    }, [])

    const [items, setItems] = useState([])

    const fetchItem = async () => {
        const data = await fetch('https://gorest.co.in/public/v1/users')

        const items = await data.json()
        console.log(items.data)
        setItems(items.data)
    }

    return (
        <div>
            {items.map( item => (
                <h1 key= {item.id}>
                    <Link to={`/shop/${item.id}`}>{item.name}</Link>
                </h1>
            ))}
        </div>
    );
}

export default Shop;
