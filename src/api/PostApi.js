import axiosClient from "./AxiosClient";


const postApi = {
    getPost: (param) => {
        const url = '/posts';
        return axiosClient.get(url, param)
    }
}

export default postApi;