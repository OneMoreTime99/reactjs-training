import hobbyReducer from "./Hobby";
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    hobby: hobbyReducer
});

export default rootReducer;