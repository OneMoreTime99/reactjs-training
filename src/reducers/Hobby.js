// Khởi tạo giá trị ban đầu cho list và activeId
const intialState = {
    list: [],
    activeId: null
}

const hobbyReducer = (state = intialState, action) => {
    switch (action.type) {
        // Đối với trường hợp type truyền vào là 'ADD_HOBBY'
        case 'ADD_HOBBY': {
            // Nên khai báo biến mới và gán giá trị của state hiện tại cho nó
            const newList = [...state.list];
            // Add giá trị mới vào biến mới này
            newList.push(action.payload);
            // Khi trả về kết quả chúng ta nên trả về cả ...state để nó sẽ lấy những giá trị ban đầu ko thay đổi
            return {
                ...state,
                list: newList,
            }
        }

        case 'SET_ACTIVE_HOBBY': {
            // Khai báo một biến mưới và gán id truyền vào cho nó
            const newActiveId = action.payload.id;
            return {
                //Lúc này giá trị active sẽ được gán
                ...state,
                activeId: newActiveId
            }
        }

        default:
            return state;
    }
}

export default hobbyReducer;